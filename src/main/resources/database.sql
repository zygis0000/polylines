create schema if not exists map collate utf8mb4_0900_ai_ci;

create table if not exists mapCoordinates
(
	idCoord int auto_increment
		primary key,
	latitude double not null,
	longitude double not null
);

