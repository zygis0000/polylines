package com.polyline.controllers;

import com.polyline.entities.MapCoordinates;
import com.polyline.service.CoordinatesLoaderService;
import com.polyline.service.MapCoordinatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
public class CoordinatesController {

    @Autowired
    private MapCoordinatesService service;

    @Autowired
    private CoordinatesLoaderService loader;

    @RequestMapping("/getAllCoordinates")
    public List<MapCoordinates> getAllCoordinates() {
        return service.getAllCoordinates();
    }

    @RequestMapping("/loadCoordinates")
    public boolean loadToDataBaseCoordinates(@RequestParam int times) throws IOException {
       return loader.loadToDataBase(times);
    }
}
