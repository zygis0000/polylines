package com.polyline.service;


import com.jayway.jsonpath.JsonPath;
import com.polyline.entities.MapCoordinates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class CoordinatesLoaderService {

    @Autowired
    private MapCoordinatesService service;

    private List<MapCoordinates> list;

    private static final String USER_AGENT = "Mozilla/5.0";

    public CoordinatesLoaderService() throws IOException {
        list= loadCoordinatesToArray(1000);
    }

    private double getRandomDoubleByRange(double min, double max) {
        Random random = new Random();
        DecimalFormat df = new DecimalFormat("##.######");
        double x = random.nextDouble() * (max - min) + min;
        return Double.valueOf(df.format(x));
    }

    private String getApiInString(double latitude, double longitude) throws IOException {
        String API_URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latitude
                + "," + longitude + "4&key=AIzaSyDXl6_-jebhF2_vVmi_J_AuXNAclsyqnIw";
        HttpURLConnection con = (HttpURLConnection) new URL(API_URL).openConnection();
        con.setRequestProperty("User-Agent", USER_AGENT);
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    private String responseCityCheck(String api) {
        String x = JsonPath.read(api, "$.plus_code.compound_code");
        String[] splited = x.split("\\s+");
        String town = "Kaunas,";
        if (splited[1].equals(town)) {
            return api;
        }
        return null;
    }

    private List<MapCoordinates> loadCoordinatesToArray(int times) throws IOException {
        List<MapCoordinates> list = new ArrayList<>();
        for (int i = 0; i < times; i++) {
            double latitude = getRandomDoubleByRange(54.835, 54.95);
            double longitude = getRandomDoubleByRange(23.785, 24.06);
            String api = getApiInString(latitude, longitude);
            if (responseCityCheck(api) != null) {
                MapCoordinates coordinates = new MapCoordinates();
                coordinates.setLatitude(latitude);
                coordinates.setLongitude(longitude);
                list.add(coordinates);
            } else {
                --i;
            }
        }
        return list;
    }

    public boolean loadToDataBase(int times) {
        try {
            long start = System.currentTimeMillis();
            List<MapCoordinates> coordinates = loadCoordinatesToArray(times);
            service.addCoordinates(coordinates);
            long end = System.currentTimeMillis();
            System.out.println("Takes: " + (end - start));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            return true;
        }
    }


}
