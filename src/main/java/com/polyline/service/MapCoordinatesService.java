package com.polyline.service;

import com.polyline.dao.MapCoordinatesDao;
import com.polyline.entities.MapCoordinates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class MapCoordinatesService implements IMapCoordinatesSevice {
    @Autowired
    private MapCoordinatesDao dao;
    @Override
    public boolean addCoordinates(List<MapCoordinates> coordinates) {
        return dao.addCoordinates(coordinates);
    }

    @Override
    public List<MapCoordinates> getAllCoordinates() {
        return dao.getAllCoordinates();
    }
}
