package com.polyline.service;

import com.polyline.entities.MapCoordinates;

import java.util.List;

public interface IMapCoordinatesSevice {
    boolean addCoordinates(List<MapCoordinates> coordinates);
    List<MapCoordinates> getAllCoordinates();
}
