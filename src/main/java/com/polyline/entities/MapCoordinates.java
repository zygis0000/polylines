package com.polyline.entities;

import org.springframework.stereotype.Service;

import java.util.Objects;
@Service
public class MapCoordinates {

  private long idCoord;
  private double latitude;
  private double longitude;


  public long getIdCoord() {
    return idCoord;
  }

  public void setIdCoord(long idCoord) {
    this.idCoord = idCoord;
  }


  public double getLatitude() {
    return latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }


  public double getLongitude() {
    return longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    MapCoordinates that = (MapCoordinates) o;
    return idCoord == that.idCoord &&
            Objects.equals(latitude, that.latitude) &&
            Objects.equals(longitude, that.longitude);
  }

  @Override
  public int hashCode() {
    return Objects.hash(idCoord, latitude, longitude);
  }
}
