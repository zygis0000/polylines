package com.polyline.dao;

import com.polyline.entities.MapCoordinates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class MapCoordinatesDao {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public boolean addCoordinates(List<MapCoordinates> coordinates ) {
        StringBuffer query = new StringBuffer("insert into map.mapCoordinates (latitude, longitude) " +
                "values ");
        for (int i = 0; i < coordinates.size(); i++) {
            query.append("( :latitude" + i + ", :longitude" + i + ")");
            if (i != coordinates.size()-1){
                query.append(",");
            }

        }
        query.append(";");
        MapSqlParameterSource param = new MapSqlParameterSource();
        for (int i = 0 ; i<coordinates.size(); i++) {
            param.addValue("latitude"+i, coordinates.get(i).getLatitude());
            param.addValue("longitude"+i, coordinates.get(i).getLongitude());
        }
        int i = jdbcTemplate.update(query.toString(), param);
        return i > 0 ? true : false;
    }

    public List<MapCoordinates> getAllCoordinates() {
        String q = "select * from mapCoordinates";
        return jdbcTemplate.query(q, this::mapCoordinates);
    }

    private List<MapCoordinates> mapCoordinates(ResultSet rs) throws SQLException {
        List<MapCoordinates> coordinatesList = new ArrayList<>();
        while (rs.next()) {
            MapCoordinates coordinates = new MapCoordinates();
            coordinates.setLatitude(rs.getDouble("latitude"));
            coordinates.setLongitude(rs.getDouble("longitude"));
            coordinatesList.add(coordinates);
        }
        return coordinatesList;
    }

}
