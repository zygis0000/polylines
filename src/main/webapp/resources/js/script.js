var map;
var path;
var coordinates = new Array();

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 54.898521, lng: 23.903597},
        zoom: 12
    });
}

function pushToArray(from, to) {
    for (var i = 0; i < from.length; i++) {
        to.push({lat: from[i].latitude, lng: from[i].longitude});
    }
    return to;
}


function createPolylines(array) {

    path = new google.maps.Polyline({
        path: array,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });
    path.setMap(map);
}

function drawPolylines() {
    var val = 0;
    val = parseInt($(".value1").val());
    if (val != 0) {
        if (!isNaN(val)) {
            $.getJSON("http://localhost:8080/getAllCoordinates", function (result) {
                if (result.length > 0) {
                    if (val > result.length) {
                        $(".message1").html("Not enought coordinates !");
                    } else if (coordinates.length < result.length || coordinates.length == 0) {
                        coordinates.length == 0;
                        var array = pushToArray(result, coordinates);
                        polylinesEngine(array, val);
                    } else {
                        polylinesEngine(coordinates, val);
                    }
                } else {
                    $(".message1").html("There is no data in database ! Please add it !");
                }
            });

        } else {
            $(".message1").html("Value is not a number !");
        }
    } else {
        $(".message1").html("Value is 0 !");
    }
}

$(".load").click(function () {
    var val = 0;
    val = parseInt($(".value").val());
    console.log(val);
    if (val != 0) {
        if (!isNaN(val)) {
            $.getJSON("http://localhost:8080/loadCoordinates?times=" + val, function (result) {
                if (result == true) {
                    $(".message").html(val + " coordinates" + " have been loaded to database !");
                } else {
                    $(".message").html("Data not loaded");
                }
            });
        } else {
            $(".message").html("Value is not a number !");
        }
    } else {
        $(".message").html("Value is 0 !");
    }
});

$(".empty").click(function () {
    if (coordinates.length != 0) {
        coordinates.length = 0;
        path.setMap(null);

    }
});


function polylinesEngine(arrayCoord, value) {
    var array = new Array();
    for (var i = 0; i < value; i++) {
        array.push(arrayCoord[i]);
    }
    if (path !== undefined) {
        path.setMap(null);
    }
    createPolylines(array);
    $(".message1").html(value + " polylines have been drawn !");
}


