<!DOCTYPE html>
<html>
<head>
    <title>Polyline</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <link rel="stylesheet" href="resources/css/style.css">
</head>
<body>
<div id="map"></div>
<div class="container">

    <div class="form">
        <h3>Generator Panel:</h3>
        <div class="generate">
            <input type="text" class="value">
            <button type="button" class="load">Generate coordinates</button>
            <p class="message"></p>
        </div>
        <h3>Draw Panel:</h3>
        <div class="generate">
            <input type="text" class="value1">
            <button type="button" class="btn" onclick="drawPolylines()">Draw Polylines</button>
            <p class="message1"></p>
        </div>
        <div class="generate">
            <button class="empty">Delete polylines</button>
        </div>
    </div>

</div>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.js"></script>
<script src="resources/js/script.js"></script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDXl6_-jebhF2_vVmi_J_AuXNAclsyqnIw&callback=initMap"
></script>
</body>
</html>
